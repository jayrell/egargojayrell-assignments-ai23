<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'JayrellEgargo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'auZH*o{^w7U &Tj8=i&h,+ni3!VPUbo?p]%.z>m(3.1fM,og%l$c6Y33ZAn1EL<D' );
define( 'SECURE_AUTH_KEY',  'jSrv[N[mT%x{c)zZ$v bp:^+IN?M9P%)(o5?_9K>nt4OHuM9$D:$sZ4kD2kLMYqU' );
define( 'LOGGED_IN_KEY',    'M_x:Jgr[$6Qj->!<jHGmI^IX$$A#eeM8#Ng4IsIrLn3|wGC{X[{+Tr0@~d,-0cA.' );
define( 'NONCE_KEY',        ' g:)C/H9|Hrim*]^3*W@+)F4uV<r] l^t(2Tjar)+pwtd1I:@1L;,M//T(C(@*u%' );
define( 'AUTH_SALT',        'nF766xLoNWpZw*dX9USL^ob?[fA1skt5548/mtauV6#)_y/C=*`t)FDQ:4XzF ;)' );
define( 'SECURE_AUTH_SALT', '?`Bj=5^s}T-wHrt3P= _o^`$O3azQ#}w5RAb.tr%YLI>&Y|Q{+t?HE%E5YH<[`;u' );
define( 'LOGGED_IN_SALT',   'r y1P{0d}TOcs.gGg .MD*KO^|L<r1yWq>kdv#D={A%YmruV;3iSEbBJ2fLz[EAW' );
define( 'NONCE_SALT',       '2_hm-$qGqy(#mwQno Cs4-!A7P|~Y7+7*zZ[Ab`4-pRX6G#E9{N2,^NMw Nks;o~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
